/**
 * list-template.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-11-05
 */
import $ from "jquery"
import axios from "axios"
import React from "react"
import WMap from "../wrapper/map-wrapper"
import ReactDOM from "react-dom"

export default class ListTemplate extends React.Component {
  constructor(props) {
    super(props)
    this.icons = new WMap()
    this.texts = new WMap()
    this.placeholders = new WMap()
    this.state = { json: [{}], sort: "", order: "ASC" }
  }
  componentWillMount() {
    this.disableSort()
    const axReq = this.requests().map(req => {
      return ( axios.get(req.url, req.params) )
    })
    axios.all(axReq).then(axios.spread(this.callback())).catch((err) => {
      console.error(err)
    })
  }
  componentDidMount() {
    this.enableSort()
  }
  requests() {
    return (
      [
        { "url": "", "params": {} }
      ]
    )
  }
  callback() {
    return ((res) => {this.setState({ "test": res.data })})
  }
  sortStatus(name, order) {
    const className = (this.state.order === "ASC") ? "fa fa-sort-asc" : "fa fa-sort-desc"
    if (this.state.sort === name) {
      return <i className={ className }></i>
    }
    return <i className={ "fa fa-sort" }></i>
  }

  enableSort() {
    const $this = this
    $("thead th").on("click", ev => {
      const id = $(ev.currentTarget).attr("id")
      if ($this.state.sort === id) {
        const order = ($this.state.order === "ASC") ? "DESC" : "ASC"
        const state = { json: $this.state.json, sort: $this.state.sort, order: order }
        $this.setState(state)
      } else {
        const state = { json: $this.state.json, sort: id, order: "ASC" }
        $this.setState(state)
      }
    });
  }
  disableSort() {
    $("thead th").off("click")
  }
  
  tableHeader() {
    return (
      <tr>
        <th>Should override tableHeader()<i className={ this.sortStatus() }></i></th>
      </tr>
    )
  }
  columnFormatter(elem) {
    return(
      <tr>
        <td>Should override columnFormatter()</td>
      </tr>
    )
  }
  render() {
    return(
      <div className="list row">
        <div className="list__title col-lg-12">
          <h2 className="page-header"><i className={ this.icons.getOrElse("title", "Set the value corresponding to the key[texts.title]") }></i>{ this.texts.getOrElse("title", "Set the value corresponding to the key[icons.title]") }</h2>
        </div>
        <div className="list__button col-lg-12 clearfix">
          <input type="text" id="searchTxt" className="form-control pull-left" placeholder={ this.placeholders.getOrElse("searchTxt", "Set the value corresponding to the key[placeholders.searchTxt]") } />
          <button id="searchBtn" className="btn btn-default pull-left" onClick={ this.searchEvt }>{ this.texts.getOrElse("searchBtn", "Set the value corresponding to the key[texts.searchBtn]") }</button>
          <button id="createBtn" className="btn btn-primary pull-right">{ this.texts.getOrElse("createBtn", "Set the value corresponding to the key[texts.createBtn]") }</button>
        </div>
        <div className="list__table col-lg-12">
          <table className="table table-bordered table-striped">
            <thead>
              { this.tableHeader() }
            </thead>
            <tbody>
              { this.state.json.map(elem => this.columnFormatter(elem)) }
            </tbody>
          </table>
        </div>

        <ul className="pagination">
          <li className="disabled"><a href="#">«</a></li>
          <li className="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li className="active"><a href="#">»</a></li>
        </ul>
      </div>
    )
  }
}
