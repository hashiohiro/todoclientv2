/**
 * map-wrapper.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-11-05
 */
export default class MapWrapper {
  constructor() {
    this.map = new Map()
  }
  set(key, value) {
    this.map.set(key, value)
  }
  getOrElse(key, defaultValue) {
    const value = this.map.get(key)
    if (value === undefined) {
      return defaultValue
    }
    return value
  }
}
