/**
 * todos-domain.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-11-05
 */
class TodosDomain  {
  path(id) {
    return `tasks/${id}`
  }
  priorityClass(level) {
    if (level === "High") {
      return "label label-danger"
    } else if (level === "Normal") {
      return "label label-success"
    } else {
      return "label label-primary"
    }
  }
}
export default new TodosDomain()
