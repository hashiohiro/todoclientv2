/**
 * users-domain.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-11-05
 */
class UsersDomain  {
  path(id) {
    return `users/${id}`
  }
}
export default new UsersDomain()
