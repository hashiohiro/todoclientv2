/**
 * unit.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-11-05
 */
class Unit  {
  days(days) {
    return (days <= 1 && days >= -1) ? " day" : " days"
  }
}
export default new Unit()
