/**
 * deadline-span.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-10-27
 */
import React from "react"
import moment from "moment"
import Unit from "../domains/unit"
export default class DeadlineSpan extends React.Component {
  constructor(props) {
    super()
    this.state = { deadline: props.deadline}
  }
  displayStyle(remainDays) {
    if (remainDays <= 1) {
      return "text-danger"
    } else if (remainDays <= 3) {
      return "text-warning"
    } else {
      return "text-success"
    }
  }
  remainTime() {
    const now = moment()
    const rem = moment(this.state.deadline).diff(now, "days")
    return (<span>{ moment(this.state.deadline).format("YYYY-MM-DD") } ( <strong className={ this.displayStyle(rem) }>{ rem }</strong>{ Unit.days(rem) } )</span>)
  }
  render() {
    const iconStyle = {"marginRight": "4px"}
    return (
      <span>
        <i className="fa fa-clock-o" style={iconStyle} aria-hidden="true"></i>
        { this.remainTime(this.state.remainHours) }
      </span>
    )
  }
}
