import React from "react"
import ReactDOM from "react-dom"
import { Link } from "react-router"

export default class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {userId: "", userName: "", active: "index"};
  }
  render() {
    return(
      <div className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <Link to="/" className="navbar-brand">Todo</Link>
            <button className="navbar-toggle" type="button" data-toggle="collapse" data-target="#header-main">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
          </div>
          <div className="collapse navbar-collapse" id="header-main">
            <ul className="nav navbar-nav">
              <li><a href="#">Overview</a></li>
              <li><Link to="/todos">Todo</Link></li>
              <li><Link to="/users">User</Link></li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}
