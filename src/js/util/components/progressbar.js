/**
 * progressbar.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-10-16
 */
import React from "react"
export default class ProgressBar extends React.Component{

  constructor(props) {
    super()
    this.state = {now: props.now, height: 6}
  }
  progressbarStyle(now) {
    return {width: now + "%"}
  }
  progressStyle(height) {
    return {height: height + "px"}
  }
  progressClass(now) {
    const isDanger = (value) => (value >= 0 && value < 30)
    const isWarning = (value) => (value >= 30 && value < 50)
    const isNormal = (value) => (value >= 50 && value < 80)
    const isGood = (value) => (value >= 80 && value <= 100)

    if (isDanger(now)) {
      return "progress-bar progress-bar-danger"
    } else if (isWarning(now)) {
      return "progress-bar progress-bar-warning"
    } else if (isNormal(now)) {
      return "progress-bar progress-bar-info"
    } else if (isGood(now)) {
      return "progress-bar progress-bar-success"
    }
  }
  className(cls) {
    return `progress ${cls}`
  }

  render() {
    return (
      <div className={ this.className() } style={this.progressStyle(this.state.height)}>
        <div className={this.progressClass(this.state.now)} role="progressbar" aria-valuenow={this.state.now} aria-valuemin="0" aria-valuemax="100" style={this.progressbarStyle(this.state.now)}>
          <span className="sr-only">{this.state.now}</span>
        </div>
      </div>
    )
  }
}
