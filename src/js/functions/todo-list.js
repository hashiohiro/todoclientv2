/**
 * todo-list.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-11-05
 */
import axios from "axios"
import React from "react"
import moment from "moment"
import Link from "react-router"
import ReactDOM from "react-dom"
import domain from "../util/domains/todos-domain"
import Deadline from "../util/components/deadline-span"
import ProgressBar from "../util/components/progressbar"
import ListTemplate from "../util/templates/list-template"


export default class TodoList extends ListTemplate {
  constructor(props) {
    super(props)
    this.state = {
      json: [],
      users: []
    }
    this.icons.set("title", "fa fa-tasks")
    this.texts.set("title", "Todo list")
    this.texts.set("searchBtn", "Search")
    this.texts.set("createBtn", "Create")
    this.placeholders.set("searchTxt", "Search words...")
  }
  tableHeader() {
    return (
      <tr>
        <th id="name">Name{ this.sortStatus("name", this.state.order) }</th>
        <th id="responsible">Responsible{ this.sortStatus("responsible", this.state.order) }</th>
        <th id="priority" className="text-center">Priority{ this.sortStatus("priority", this.state.order) }</th>
        <th id="progress" className="text-center">Progress{ this.sortStatus("progress", this.state.order) }</th>
        <th id="deadline_at">Deadline{ this.sortStatus("deadline_at", this.state.order) }</th>
      </tr>
    )
  }
  columnFormatter(elem) {
    const respo = this.state.users.find(usr => {
      return usr.url === elem.responsible_url
    })
    const usrPath = (id) => {return `users/${id}`}
    const deadlineTxt = moment(elem.deadline_at).format("YYYY-MM-DD")
    const respoTxt = respo !== undefined ? <a href={ usrPath(respo.id) }>{ respo.name }</a> : ""
    return (
      <tr key={ elem.id }>
        <td><i className="fa fa-tasks"></i>{ <a href={ domain.path(elem.id) }>{ elem.name }</a> }</td>
        <td><i className="fa fa-user"></i>{ respoTxt }</td>
        <td className="text-center todo-priority"><span className={ domain.priorityClass(elem.priority) }>{ elem.priority }</span></td>
        <td className="text-center todo-progress"><ProgressBar now={ elem.progress } /></td>
        <td><Deadline deadline={ deadlineTxt }/></td>
      </tr>
    )
  }
  requests() {
    return (
      [
        { "url": "http://localhost:3000/v2/todos", "params": {} },
        { "url": "http://localhost:3000/v2/users", "params": {} }
      ]
    )
  }
  callback() {
    return ((todos, users) => {this.setState({"json": todos.data, "users": users.data})})
  }
}
