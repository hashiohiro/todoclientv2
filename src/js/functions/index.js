/**
 * index.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-10-15
 */
import React from "react"
import ReactDOM from "react-dom"

export default class Index extends React.Component {
  constructor(props) {
    super(props)
    alert("test")
  }
  render() {
    return(
      <div className="container">
        test
      </div>
    )
  }
}
