/**
 * user-list.js
 * Author: Hiroshi Hashimoto
 */
import axios from "axios"
import React from "react"
import moment from "moment"
import Link from "react-router"
import ReactDOM from "react-dom"
import domain from "../util/domains/users-domain"
import Deadline from "../util/components/deadline-span"
import ProgressBar from "../util/components/progressbar"
import ListTemplate from "../util/templates/list-template"


export default class TodoList extends ListTemplate {
  constructor(props) {
    super(props)
    this.state = { json: [] }
    this.icons.set("title", "fa fa-user")
    this.texts.set("title", "User list")
    this.texts.set("searchBtn", "Search")
    this.texts.set("createBtn", "Create")
    this.placeholders.set("searchTxt", "Search words...")
  }

  tableHeader() {
    return (
      <tr>
        <th id="name">Name{ this.sortStatus("name", this.state.order) }</th>
        <th id="mail">E-mail{ this.sortStatus("mail", this.state.order) }</th>
        <th id="dep">Department{ this.sortStatus("dep", this.state.order) }</th>
        <th id="position" className="text-center">Position{ this.sortStatus("position", this.state.order) }</th>
        <th id="age" className="text-center">Age{ this.sortStatus("age", this.state.order) }</th>
      </tr>
    )
  }
  columnFormatter(elem) {
    const mailto = `mailto:${ elem.mail }`
    const birthday = moment(elem.birthday).format("YYYY-MM-DD")
    const age = moment().subtract(moment(elem.birthday).format("YYYY-MM-DD"), "years")
    return (
      <tr key={ elem.id }>
        <td><i className="fa fa-user"></i>{ <a href={ domain.path(elem.id) }>{ elem.name }</a> }</td>
        <td><i className="fa fa-envelope"></i><a href={ mailto }>{ elem.mail }</a></td>
        <td className="user-department"><span><i className="fa fa-building"></i>{ elem.dep }</span></td>
        <td className="text-center user-position"><i className="fa fa-sitemap"></i>{ elem.position }</td>
        <td className="text-center user-age"><i className="fa fa-birthday-cake"></i>{ birthday }</td>
      </tr>
    )
  }
  requests() {
    return (
      [
        { "url": "http://localhost:3000/v2/users", "params": {} }
      ]
    )
  }
  callback() {
    return ((users) => {this.setState({"json": users.data})})
  }
}
