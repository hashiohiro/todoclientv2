/**
 * app.js
 * Author: Hiroshi Hashimoto
 * Date: 2016-10-15
 */
import React from "react"
import ReactDOM from "react-dom"
import Index from "./functions/index.js"
import TodoList from "./functions/todo-list.js"
import UserList from "./functions/user-list.js"
import TodoEdit from "./functions/todo-edit.js"
import Header from "./util/components/header.js"
import { Router, Route, IndexRoute, Link, browserHistory } from "react-router"

class App extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return(
      <div>
        <Header />
        <div className="container">
          { this.props.children }
        </div>
      </div>
    )
  }
}

ReactDOM.render((
  <Router history={ browserHistory }>
    <Route path="/" component={ App }>
      <IndexRoute component={ UserList } />
      <Route path="users" component={ UserList } />
      <Route path="todos" component={ TodoList } />
    </Route>
  </Router>
), document.getElementById("app"))
