/**
 * gulpfile.js

 * Author: Hiroshi Hashimoto
 * create: 2016-10-15
 */
const gulp = require("gulp")
const sass = require("gulp-sass")
const autoprefixer = require("gulp-autoprefixer")
const frontnote = require("gulp-frontnote")
const uglify = require("gulp-uglify")
const babel = require("gulp-babel")
const webserver = require("gulp-webserver")
const plumber = require("gulp-plumber")
const browserify = require("browserify")
const babelify = require("babelify")
const cleanCss = require("gulp-clean-css")
const source = require("vinyl-source-stream")

/**
 * To start webserver
 */
gulp.task("server", () => {
  gulp.src(".")
    .pipe(webserver({
      livereload: true,
      open: true
  }))
})

/**
 * sass2css + create styleguide
 * source: src/css/*
 * destination: dest/css/*.css
 * style guide: guide/index.html
 */
gulp.task("sass", () => {
  gulp.src("src/css/**/*sass")
    .pipe(plumber())
    .pipe(frontnote({
      css: 'dist/css/*css'
    }))
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest("./dest/css"))
  gulp.src("node_modules/font-awesome/fonts/*")
    .pipe(gulp.dest("./dest/fonts"))
})

/**
 * ES6 to ES5
 * source: src/js/*
 * destination: dest/js/app.js
 *
 * Todo: add css minifier
 */
gulp.task("js", () => {
  browserify("src/js/app.js", {debug: true})
    .transform(babelify, {presets: ["es2015", "react"]})
    .bundle()
    .on("error", (err) => {console.log("Error :" + err.message)})
    .pipe(source("app.js"))
    .pipe(gulp.dest("./dest/js"))
})

/**
 * Default
 * To watch the change of js and css
 */
gulp.task("default", ["server"], () => {
  gulp.watch("src/js/**/*.js", ["js"])
  gulp.watch("src/css/**/*.sass", ["sass"])
})
